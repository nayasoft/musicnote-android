package com.musicnoteapp.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import android.util.Log;
/**
 * @author nbharath
 * 
 *         This class is for File Uploading.
 * 
 */ 
	public class FileUpload {	  
		 
	    public  static void onCreate(String url,String fileName) {
	      
	        try {
	           
	            executeMultipartPost(url,fileName);
	           
	          
	        } catch (Exception e) {
	            Log.e(e.getClass().getName(), e.getMessage());
	        }
	    }
	 
	   private static void executeMultipartPost(String url,String fileName) throws Exception {
	        try {
	       
	            HttpClient httpClient = new DefaultHttpClient();
	            HttpPost postRequest = new HttpPost(url);
	        
	             File file= new File(fileName);
	             FileBody bin = new FileBody(file);
	            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
	            reqEntity.addPart("uploaded",bin);
	            reqEntity.addPart("photoCaption", new StringBody("sfsdfsdf"));
	            postRequest.setEntity(reqEntity);
	            HttpResponse response = httpClient.execute(postRequest);
	            BufferedReader reader = new BufferedReader(new InputStreamReader(
	                    response.getEntity().getContent(), "UTF-8"));
	            String sResponse;
	            StringBuilder s = new StringBuilder();
	 
	            while ((sResponse = reader.readLine()) != null) {
	                s = s.append(sResponse);
	            }	 
	        } catch (Exception e) {
	          
	            Log.e(e.getClass().getName(), e.getMessage());
	        }
	    }
	}