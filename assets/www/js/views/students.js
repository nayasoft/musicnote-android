window.StudentsView = Backbone.View.extend({
	initialize:function () {
    console.log('Initializing Students View');
	},

	render:function () {
	    $('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.webside').css( "background-color","" );	
	    $('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.sideBarContact').css( "background-color","rgb(40, 135, 189)" );
	  
		$(this.el).html(this.template());
		return this;
	}
   
});
