window.RegistrationView = Backbone.View.extend( {
	initialize : function() {
		console.log('Initializing Registration View');
		
	},
	events:{
		'submit #contact-form' :'createUser'
	},
	render : function() {
		$(this.el).html(this.template());
		return this;
	}
	,
	createUser:function(event)
	{
		createUsers();
		event.preventDefault();
	}
	
});