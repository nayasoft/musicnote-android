window.SideBar = Backbone.View.extend({

    initialize:function () {
        console.log('Initializing Side Bar');
    },

    render:function () {
        $(this.el).html(this.template());
        return this;
    },
    
    events: {
    	"click .invoice": "invoice",
    	"click .recordLesson": "recordLesson",
    	"click .reminders": "reminders",
    	"click .messages": "messages",
    	"click .students": "students",
		"click .calendar": "calendar",
		"click .crowd": "crowd",
		"click #music":  function(e) {
    		//this.render();
    	 	this.music(e);
    	},
		"click #schedule": function(e) {
    		//this.render();
    	 	this.schedule(e);
    	},
		"click #memos": function(e) {
    		//this.render();
    	 	this.bill(e);
    	},
    	"click .sideBarRecord": "recordLesson",
    	"click .sideBarContact": "students",
		"click .sideBarCrowd": "crowd",
		"click .sideBarNotes":  function(e) {
    		//this.render();
    	 	this.music(e);
    	},
		"click .sideBarMenuSchedule": function(e) {
    		//this.render();
    	 	this.schedule(e);
    	},
		"click .sideBarMemos": function(e) {
    		//this.render();
    	 	this.bill(e);
    	}
    	
    },
    	 
    invoice: function(e){
    	e.preventDefault();
    	app.navigate("invoice",{trigger:true});
    },
    
    recordLesson: function(e){
    	e.preventDefault();
    	app.navigate("recordlesson",{trigger:true});
    },
    
    reminders: function(e){
    	e.preventDefault();
    	app.navigate("reminders",{trigger:true});
    },
    
    messages: function(e){
    	e.preventDefault();
    	app.navigate("messages",{trigger:true});
    },
    
    music: function(e){
    	e.preventDefault();
    	listType="music";
    	app.navigate("music",{trigger:true});
    },
    schedule: function(e){
    	e.preventDefault();
    	listType="schedule";
    	app.navigate("schedule",{trigger:true});
    },
    bill: function(e){
    	e.preventDefault();
    	listType="bill";
    	app.navigate("memos",{trigger:true});
    },
    
    students: function(e){
    	e.preventDefault();
    	//listType="";
    	app.navigate("students",{trigger:true});
    },
	 calendar: function(e){
    	e.preventDefault();
    	app.navigate("calendar",{trigger:true});
    },
    crowd: function(e){
    	e.preventDefault();
    	listType="crowd";
    	app.navigate("crowd",{trigger:true});
    }
});