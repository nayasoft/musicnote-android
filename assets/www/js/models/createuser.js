var email="";
var regMusicnote="";
var regMusicnoteIn="";
var warningErrorFlag;
var facebookId='';
function regMusicnoteGenerator(){
	var url = urlForServer+"user/newToken";
	var musicnoteAn=$.base64.encode(url);
	$.ajax({
		
		headers:{"Mn-Callers1":musicnoteAn},

		type : 'POST',
		url : url,
		//data : params,
		success : function(responseText) {	
		if(responseText!='' && responseText!=null){
		var data=$.parseJSON(responseText);
		regMusicnote=data.token;
		regMusicnoteIn=data.datess;
		getSecurityQuestion();
		}
	},
		error:function(responsetext){
		alert('Please try again later');
	}
	});
	}
function forgetPassTokenGenerator(){
	var url = urlForServer+"user/newToken";
	var musicnoteAn=$.base64.encode(url);
	$.ajax({
		
		headers:{"Mn-Callers1":musicnoteAn},

		type : 'POST',
		url : url,
		//data : params,
		success : function(responseText) {	
		if(responseText!='' && responseText!=null){
		var data=$.parseJSON(responseText);
		regMusicnote=data.token;
		regMusicnoteIn=data.datess;
		}
	},
		error:function(responsetext){
		alert('Please try again later');
	}
	});
	}
	function getSecurityQuestion(){
		var url = urlForServer+"user/getSecurityQuestion";
		
		$.ajax({
			
			headers:{
				"Mn-Callers1":regMusicnote ,
				"Mn-time":regMusicnoteIn
				},

			type : 'POST',
			url : url,
			//data : params,
			success : function(responseText) {	
			if(responseText!='' && responseText!=null){
			var split=responseText.split('~');
			$('#securityBox').empty();
			$('#securitySelect').empty();
			$('#securitySelect').append('<option value="0">Please select a security Question</option>');
			for(var i=0;i<split.length;i++){
			var quesId=split[i].split('-');	
			$('#securitySelect').append('<option value="'+quesId[1]+'">'+quesId[0]+'</option>');		
			}

			}
		},
			error:function(responsetext){
			alert('Please try again later');
		}
		});
	}
$(document).ready( function() {
	//notification default
		$("#noteMail").attr('checked', true);
		$("#eventMail").attr('checked', true);
		$("#memoMail").attr('checked', true);
		$("#contactMail").attr('checked', true);
		$("#crowdMail").attr('checked', true);
		$("#dueDateMail").attr('checked', true);
		
	$("#password").keyup(function (e) {
		  $(this).val(function(i, v) { return v.replace(/ /g,""); });
		});
	$("#confirmpassword").keyup(function (e) {
		  $(this).val(function(i, v) { return v.replace(/ /g,""); });
		});
	
	$('#role').change(function(){
		var userRole=$('#role').val();
		if(userRole=="Music Student"){
			$("#manditorySelect").hide();
		}else{
			$("#manditorySelect").show();
			}
		
	});
	
	$('#securitySelect').change(function(){
		if($(this).val()!='0'){	
		$('#securityBox').removeAttr('disabled');
		}else{
		$('#securityBox').attr('disabled','disabled' );
		$('#securityBox').val('');
		}

		
				
		});
var spinner = $( "#inviteUsers" ).spinner({
	min: 1,
    max: 200,
   option:1
});
$('.ui-spinner-button').click(function() {
	   $(this).siblings('input').change();
	});
$('#inviteUsers').change(function(){
	$('#payForDiv').empty();	
	var value=$('#inviteUsers').val();
	if(value=='' || value==null){
		value='1';
	}	
$('#payForDiv').append("You Have to pay For "+value+" users");	
});

	
	$('ul.nav.nav-pills li a').click(function() {
		$(this).parent().addClass('active').siblings().removeClass('active');
	});
	
	$( "#dob" ).datepicker();

	$("#okviewterms").click(function() {
		$("#viewUserTerms").modal('hide');
	});
	$( "#updateUser" ).click(function() {
		//alert("click");
		updateUsers();
	});
	$("#editProfileview").click(function() {
		//$("#profileImg").attr("src", "pics/fb5527.PNG");
		$("#uploadProfileModal").modal('show');
		
	});
	
	$( "#cancelUser" ).click(function() {
		$('#main').hide();
		$('#sidecontent').show();
		$('#footer').show();
		$('#header').show();
		app.navigate("home", {
			trigger : true
		});
	});
	$("#createUser").click(function() {
		if(FBuserLogin){
			validateEmail();
		}
	createUsers();
	});
	$('#role').change(function(){
		var userRole=$('#role').val();
		if(userRole=="Music Student"){
			$("#manditorySelect").attr("style","display:none");
			$("#emailidDiv").attr("style","display:none");
			$("#emailfontstyle").attr("style","margin-left:7px;");
			$("#emailValidateError").attr("style", "display:none");
		}else{
			$("#manditorySelect").removeAttr("style");
			$("#emailfontstyle").removeAttr("style");
			//$("#emailidDiv").removeAttr("style");
			}
		
	});
	
	
	
	
	
	$("#mailNotificationList").click(function(){
	
	
	/*$("#noteMail").attr('checked', true);
	$("#eventMail").attr('checked', true);
	$("#memoMail").attr('checked', true);
	$("#contactMail").attr('checked', true);
	$("#crowdMail").attr('checked', true);
	
	
	//$("#sharedNote").attr('checked', true);
	//$("#sharedEvent").attr('checked', true);
	//$("#sharedMemo").attr('checked', true);
	$("#dueDateMail").attr('checked', true);
	//$("#commentsNote").attr('checked', true);
	//$("#commentsMemo").attr('checked', true);*/
	
	$("#chooseNotificationModel").modal('show');
	

	
});

$("#pmailNotificationList").click(function(){
	
	


    var url = urlForServer+"user/mailConfig/"+userId;
	
    
	
	$.ajax({
		headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},	
	type : 'POST',
	url : url,
	success : function(responseText) {
	var data = jQuery.parseJSON(responseText);
	  if(data!=null && data!='')
		{
		  for ( var i = 0; i < data.length; i++) {
		  
		    if(data[i].noteBasisMail=='true')
		    {
		    	  $("#noteMail").attr('checked', true);
		    }else
		    {
		    	  $("#noteMail").attr('checked', false);
		    }
		    if(data[i].eventBasisMail=='true')
		    {
		    	  $("#eventMail").attr('checked', true);
		    }else
		    {
		    	  $("#eventMail").attr('checked', false);
		    }
		    if(data[i].memoBasisMail=='true')
		    {
		    	  $("#memoMail").attr('checked', true);
		    }else
		    {
		    	  $("#memoMail").attr('checked', false);
		    }
		    if(data[i].contactBasisMail=='true')
		    {
		    	  $("#contactMail").attr('checked', true);
		    }else
		    {
		    	  $("#contactMail").attr('checked', false);
		    }
		    if(data[i].crowdBasisMail=='true')
		    {
		    	  $("#crowdMail").attr('checked', true);
		    }else
		    {
		    	  $("#crowdMail").attr('checked', false);
		    }
		    
		    if(data[i].dueDateMail=='true')
		    {
		    	  $("#dueDateMail").attr('checked', true);
		    }else
		    {
		    	  $("#dueDateMail").attr('checked', false);
		    }
		   		  
			
		  }
			$("#chooseNotificationModel").modal('show');

		}else
		{
			
		}
	
},
error : function() {
	console.log("<-------error returned for User details -------> ");
	}
});   
 
});

$("#paddMailNotifyList").click(function(){
	
	 var pnoteMail=false;
	  var peventMail=false;
	  var pmemoMail=false;
	  var pcontactMail=false;
	  var pcrowdMail=false;
	  var pdueDateMail=false;
	  if($('input:checkbox[name=noteMail]:checked').val()!='' && $('input:checkbox[name=noteMail]:checked').val()=="noteBasis")
	  {
	  	pnoteMail=true;
	  }
	  else
	  {
		  pnoteMail=false; 
	  }
	  if($('input:checkbox[name=eventMail]:checked').val()!='' && $('input:checkbox[name=eventMail]:checked').val()=="eventBasis")
	  {
	  	peventMail=true;
	  }
	  else
	  {
		  peventMail=false; 
	  }
	  if($('input:checkbox[name=memoMail]:checked').val()!='' && $('input:checkbox[name=memoMail]:checked').val()=="memoBasis")
	  {
	  	 pmemoMail=true;
	  }
	  else
	  {
		  pmemoMail=false; 
	  }
	  if($('input:checkbox[name=contactMail]:checked').val()!='' && $('input:checkbox[name=contactMail]:checked').val()=="contactBasis")
	  {
	  	pcontactMail=true;
	  }
	  else
	  {
		  pcontactMail=false; 
	  }
	  if($('input:checkbox[name=crowdMail]:checked').val()!='' && $('input:checkbox[name=crowdMail]:checked').val()=="crowdBasis")
	  {
	  	pcrowdMail=true;
	  }
	  else
	  {
		  pcrowdMail=false; 
	  }
	  if($('input:checkbox[name=dueDateMail]:checked').val()!='' && $('input:checkbox[name=dueDateMail]:checked').val()=="dueDateMail")
	  {
		  pdueDateMail=true;
	  }
	  else
	  {
		  pdueDateMail=false; 
	  }
	 
	  var url = urlForServer+"user/updateMailConfig/"+userId;
		
	  var datastr = '{"noteMail":"'+pnoteMail+'","eventMail":"'+peventMail+'","memoMail":"'+pmemoMail+'","contactMail":"'+pcontactMail+'","crowdMail":"'+pcrowdMail+'","dueDateMail":"'+pdueDateMail+'"}';
         console.log("<-------Sdatastr -------> " + datastr);
       var params = encodeURIComponent(datastr);
		$.ajax({
			headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},	
		type : 'POST',
		url : url,
		data : params,
		success : function(responseText) {
		//var data = jQuery.parseJSON(responseText);
		if(responseText!='' && responseText=='success')
		{
			$("#chooseNotificationModel").modal('hide');
		}else
		{
			//$("#chooseNotificationModel").modal('show');
		}
		 
		
	},
	error : function() {
		console.log("<-------error returned for User details -------> ");
		}
});

});
$("#addMailNotifyList").click(function(){
	
	//var noteMail=$('input:checkbox[name=noteMail]:checked').val();
	//alert("this"+noteMail);
	
	$("#chooseNotificationModel").modal('hide');
});
	 
});



function createUserTest(){
	console.log("<-------Sdatastr -------> ");
alert("testing");
var firstName = "veera";
var lastName = "prathaban";
var gender = "Male";
var emailId = "veera@gmail.com";
var dob = "03-04-1989";
var fbusername = "veeraprathaban.p";
var password = "nayatesting";
var confirmpassword = "veera";
var role = "Teacher";
var address = "Address 1";
var contactNumber = "9677739441";
var classId = "pino";
var experience = "2";
var payperHours = "45$";
var qulification = "BE";
var lesson = "Pino";
var notification= "yes";
var  notificationFlag;
if(notification.checked==true)
{
	notificationFlag="yes";
}
else
{
	notificationFlag="no";

}
if (firstName != null && firstName != '' && lastName != null
		&& lastName != '' && emailId != null && emailId != ''
		&& gender != null && gender != '' && dob != null && dob != ''
		&& fbusername != null && fbusername != '' && role != null
		&& role != '' && address!=null && address!='' 
		&& contactNumber!=null && contactNumber!='') {
	var url = urlForServer + "user/createUser";
	var datastr = '{"userFirstName":"' + firstName + '","userLastName":"'
			+ lastName + '","emailId":"' + emailId + '","gender":"'
			+ gender + '","userName":"' + fbusername + '","userRole":"' + role
			+ '","birthDate":"' + dob + '","password":"' + password
			+ '","address":"' + address + '","classId":"' + classId + '","notificationFlag":"' + notificationFlag + '","noteCreateBasedOn":"private"' 
			+ '","experience":"' + experience + '","payperHours":"' + payperHours 
			+ '","qulification":"' + qulification + '","lesson":"' + lesson 
			+ '","contactNumber":"'+ contactNumber + '"}';
	console.log("<-------Sdatastr -------> " + datastr);
	var params = encodeURIComponent(datastr);
	$
			.ajax({
				type : 'POST',
				url : url,
				data : params,
				success : function(responseText) {
					console
							.log("<-------Scuccessfully created user with response as -------> "
									+ responseText);
					$('#main').hide();
					$('#sidecontent').show();
					$('#footer').show();
					$('#header').show();
					app.navigate("home", {
						trigger : true
					});
					var data = jQuery.parseJSON(responseText);
					
					fetchUsersList();
				},
				error : function() {
					console
							.log("<-------Error returned while creating user-------> ");
				}
			});
} else {
	app.navigate("registration", {
		trigger : true
	});
}
}







function createUsers()
{
var firstName = $("#userfirstname").val().trim();
var lastName = $("#userlastname").val().trim();
//var gender = $("#gender").val();
var emailId = $("#emailid").val();
	
	
var securityId=$('#securitySelect').val()+'~'+$('#securityBox').val();
var fbusername = $("#fbusername").val();
var password = $("#password").val();
var confirmpassword = $("#confirmpassword").val();
var role = $("#role").val();
//var contactNumber = $("#contactNo").val();






var instrument=$('#instrument').val();
var skillLevel=$('#skillLevel').val();
var favoriteMusic=$('#favoriteMusic').val();
var termscheck=$('#agreeterms').is(':checked');
if(termscheck){
	$("#termsdecline").attr("style", "display:none");
}else{
	$("#termsdecline").attr("style", "display:block;font-weight: normal;line-height: 17px;margin: 0 0 0 28px;margin-top:5px;");
}
var notification= $('input:radio[name=notificationFlag]:checked').val();
var securityBox=$('#securityBox').val();
var secure=$('.securityRegBox').val();

if(secure=="" ){
	$("#dialogempty1").attr("style", "display:block;color:red;");
	return false;
}else{
	$("#dialogempty1").attr("style", "display:none");
}

if(firstName!=null && firstName!='')
{

      $("#userfirstnameDiv").attr("style", "display:none");
	  $("#userfirstnameDiv").text("");  
}
else
{
 $("#userfirstnameDiv").attr("style", "display:block");
 $("#userfirstnameDiv").text("Field cannot be blank").css({"color":"red"});
}


if(lastName!=null && lastName!='')
{

      $("#userlastnameDiv").attr("style", "display:none");
	  $("#userlastnameDiv").text("");  
}
else
{
 $("#userlastnameDiv").attr("style", "display:block");
 $("#userlastnameDiv").text("Field cannot be blank").css({"color":"red"});
}



if(fbusername!=null && fbusername!='')
{

      $("#fbusernameDiv").attr("style", "display:none");
	  $("#fbusernameDiv").text("");  
}
else
{
	$("#userDialog").attr("style", "display:none");
 $("#fbusernameDiv").attr("style", "display:block");
 $("#fbusernameDiv").text("Field cannot be blank").css({"color":"red"});
}


if(password!=null && password!='')
{

      $("#passwordDiv").attr("style", "display:none");
	  $("#passwordDiv").text("");  
}
else
{
 $("#passwordDiv").attr("style", "display:block");
 $("#passwordDiv").text("Field cannot be blank").css({"color":"red"});
}


if(confirmpassword!=null && confirmpassword!='')
{

      $("#confirmpasswordDiv").attr("style", "display:none");
	  $("#confirmpasswordDiv").text(""); 
   	  
}
else
{
 $("#confirmpasswordDiv").attr("style", "display:block");
 $( "#confirmDiv" ).attr('style','display : none');
 $("#confirmpasswordDiv").text("Field cannot be blank").css({"color":"red"});
}
var validEmail=true;
if(emailId!=null && emailId!='')
	{
	
		  $("#emailidDiv").attr("style", "display:none");
	      $("#emailidDiv").text("");  
		    var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/ 
		    isvalid = reg.test(emailId); 
		    if(isvalid==false)
		    {
		    	validEmail=false;
		    	$( "#emailValidateError" ).attr('style','display:none');
		    	$("#validationEmailidDiv").attr("style", "display:block");
				$("#validationEmailidDiv").text("Invalid email").css({"color":"red"});
				return false;
		    }
		    else
		    {
		    	$("#validationEmailidDiv").attr("style", "display:none");
				$("#validationEmailidDiv").text("");
		    }
		    
		   
		   
	}else if((role=="Music Student")&&(emailId==null || emailId=='')){
		$("#validationEmailidDiv").attr("style", "display:none");
		$("#validationEmailidDiv").text("");
		$("#emailidDiv").attr("style", "display:none");
		
	}
	else
	{
		$("#emailidDiv").attr("style", "display:block");
		$("#emailidDiv").text("Field cannot be blank").css({"color":"red"});
		return false;
	}
	 
/*var crowdNotification=$('input:radio[name=crowdnotificationFlag]:checked').val();
if(crowdNotification=="True")
	crowdNotification=true;
else
	crowdNotification=false;
*/

$("#UserEmail").text(emailId);


var email='"emailId":"' + emailId + '"';
var userRole=$('#role').val();
if((userRole=="Music Student")&&(emailId=="")){
	emailWarningFlag=false;
	emailFlag=false;
	email='"empty":"empty"';
}
if((userRole!="Music Student")&&(emailId=="")){
	 $( "#dialogempty" ).attr("style", "display:block");
}
var noteMail=false;
var eventMail=false;
var memoMail=false;
var contactMail=false;
var crowdMail=false;
var dueDateMail=false;
if($('input:checkbox[name=noteMail]:checked').val()!='' && $('input:checkbox[name=noteMail]:checked').val()=="noteBasis")
{
	noteMail=true;
}
else
{
	noteMail=false;
}
if($('input:checkbox[name=eventMail]:checked').val()!='' && $('input:checkbox[name=eventMail]:checked').val()=="eventBasis")
{
	eventMail=true;
}
else
{
	eventMail=false;
}
if($('input:checkbox[name=memoMail]:checked').val()!='' && $('input:checkbox[name=memoMail]:checked').val()=="memoBasis")
{
	 memoMail=true;
}
else
{
	memoMail=false;
}
if($('input:checkbox[name=contactMail]:checked').val()!='' && $('input:checkbox[name=contactMail]:checked').val()=="contactBasis")
{
	contactMail=true;
}
else
{
	contactMail=false;
}
if($('input:checkbox[name=crowdMail]:checked').val()!='' && $('input:checkbox[name=crowdMail]:checked').val()=="crowdBasis")
{
	crowdMail=true;
}
else
{
	crowdMail=false;
}

if($('input:checkbox[name=dueDateMail]:checked').val()!='' && $('input:checkbox[name=dueDateMail]:checked').val()=="dueDateMail")
{
	dueDateMail=true;
}
else
{
	dueDateMail=false;
}

if(FBuserLogin){
	facebookId=$("#facebookId").val();
	userFalg=false;
	userWarningFalg=false;
}else{
	facebookId="empty";
}
if(password==confirmpassword && emailFlag==false && userFalg==false && termscheck==true && passWarningFalg==false && emailWarningFlag==false&& userWarningFalg==false )
{
/*$('#id3').dialog({
    //autoOpen: false,
    width: 600,
    buttons: {
      "Ok": function () {
	
	     $(this).dialog("close");*/
		 
		 /////
		var date = new Date();
		var d = date.toTimeString(); 
		d = d.split("GMT");
		 d = d[1].split(" ");
		var offSet=d[0];
		////////////
		 
	/*if (firstName != null && firstName != '' && lastName != null
			&& lastName != '' && emailId != null && emailId != ''
			&& fbusername != null && fbusername != '' && role != null
			&& role != '' && validEmail==true) {*/
		if (firstName != null && firstName != '' && lastName != null
				&& lastName != '' 
				&& fbusername != null && fbusername != '' && role != null
				&& role != '') {
	    var url = urlForServer + "user/createUser";
		var datastr = '{"userFirstName":"' + firstName + '","userLastName":"'
				+ lastName + '","userName":"' + fbusername +
				'","userRole":"' + role + '","password":"' + password + '","favoriteMusic":"' + favoriteMusic
				+ '","skillLevel":"' + skillLevel + '","instrument":"' + instrument + '","offSet":"' + offSet
				+ '","termscheck":"'+termscheck+'","noteCreateBasedOn":"private","securityId":"'+securityId+'", '+ email + ',"noteMail":"'+noteMail+'","eventMail":"'+eventMail+'","memoMail":"'+memoMail+'","contactMail":"'+contactMail+'","crowdMail":"'+crowdMail+'","dueDateMail":"'+dueDateMail+'","facebookId":"'+facebookId+'"}';
		console.log("<-------Sdatastr -------> " + datastr);
		var params = encodeURIComponent(datastr);
		
		$('#regmsgModal').modal('show');
		$("#regmsg-header-span").text("Creating new user");
		$("#regmodal-message").text("  Please wait.This may take a moment");
		$("#schedulestatusicon").remove();
		$("#regmsg-header-span").text("Creating new user");
		$("#regmodal-message").text(" Please wait.This may take a moment");
		$("#schedulestatusicon").remove();
		
		//setTimeout(function(){
			//$('#regmsgModal').modal('hide');	
		//},15000);
		$
				.ajax({
					headers:{
					"Mn-Callers1":regMusicnote ,
					"Mn-time":regMusicnoteIn
					},
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) {
					console.log('!----- response after the regs'+responseText);
					var data = jQuery.parseJSON(responseText);
					if(data.userId!="0"){
						$('#regmsgModal').modal('hide');	
						userId=data.userId;
						
						/*fetchUsersList();
						doUserAppDetils();
						fetchUsers();
						if(is_chrome || is_safari)
						    $.jStorage.set("loginId",data.userId);
						    else
						    document.cookie=data.userId;   */ 
			               
						console
								.log("<-------Scuccessfully created user with response as -------> "
										+ data.userId);
						var userRole=$('#role').val();
						var emailIdAF = $("#emailid").val();
						
						if((userRole=="Music Student")&&(emailIdAF=='')){
							//alert("userRole:::"+userRole);
							app.navigate("", {
								trigger : true
								
							});
						}
						
						else{
						
						
						//var username=data.username;
						$('#emailVerifyModal').prependTo('body').modal('toggle');
						$("#emailVerify-header-span").empty();
						$("#verifyResetButtons").hide();
						//$("#emailresetVerify-message").hide();
						$("#emailCode").empty();
						$("#emailCode").hide();
						$("#emailButton").hide();
						$("#emailVerify").show();
						$("#emailVerify-message").empty();
						$("#emailVerify-message").css("color","");
						$("#emailVerify-message").css("font-weight","");
						$("#emailVerify-header-span").text("Email Verification Sent");
						$("#emailVerify-message").text("Please check your Email. Click the link and use the verification code to log in to complete your registration!");
						$("#emailOkButton").append('<button class="modalBtn fontStyle" id="emailVerify" >OK</button>');
						$("#emailVerify").click(function(){$('#emailVerifyModal').modal('hide');});
						setTimeout(function(){getRequests();},1000);
						
						$("#userfirstname").val("");
						$("#userlastname").val("");
						$("#emailid").val("");
						$("#agreeterms").attr('checked', false); 
						$("#fbusername").val("");
						$("#password").val("");
						$("#confirmpassword").val("");
					   // $('#main').hide();
						//$('#sidecontent').show();
						//$('#footer').show();
					//	$('#header').show();
						//$('#sidecontent').parent().css( "background-color","#DDDDDD");
						/*if(FBuserLogin){
							logoutFB();
						}*/
						app.navigate("", {
							trigger : true
							
						});
						userCreationMailSend(userId);
						}
						/*$('#welcomemsg').attr('style','display:block;font-family: Helvetica Neue;font-size:14px;');
/////////////////////////////////////////////
						var userSharedIdString =data.sharedIdSet;
						
						if(userSharedIdString.indexOf("[") != -1){
							userSharedIdString = userSharedIdString.substring(userSharedIdString.indexOf("[")+1,userSharedIdString.length);
						}
						if(userSharedIdString.indexOf("]") != -1){
							userSharedIdString = userSharedIdString.substring(0,userSharedIdString.indexOf("]"));
						}
						if(userSharedIdString.indexOf(",") != -1){
							userIdSharedIdSetArray = userSharedIdString.split(',');
						
						}else{
							if(userSharedIdString.trim() != ''){
								userIdSharedIdSetArray = userSharedIdString.split(',');
							}
						}
						
						//////////////////////////////////////////////
						if(data.requestPending){
							var userIdSetArray= new Array();
							var divFrdAc = "";
							var userIdString =data.userIdSet;
							if(userIdString.indexOf("[") != -1){
								userIdString = userIdString.substring(userIdString.indexOf("[")+1,userIdString.length);
							}
							if(userIdString.indexOf("]") != -1){
								userIdString = userIdString.substring(0,userIdString.indexOf("]"));
							}
							if(userIdString.indexOf(",") != -1){
								userIdSetArray = userIdString.split(',');
							
							}else{
								if(userIdString.trim() != ''){
									userIdSetArray = userIdString.split(',');
								}
							}
							if(userIdSetArray.length > 0 && userIdSetArray[0]!=null ){
								setTimeout(function(){
									divFrdAc = createFriendAceeptDivByMail(userIdSetArray,userIdSharedIdSetArray);
									$('#mailSharingModel').children('.modalBody').children().append("<br>"+divFrdAc+"<br>")
									$('#mailSharingModel').prependTo('body').modal('show');
								},800);
							}
						}*/
					    // $('#id2').data('propagationStopped', true);
				       // $('#id2').triggerHandler(event);
					} else{
						app.navigate("registration", {
							trigger : true
						});
					}
					},
					error : function() {
						console
								.log("<-------Error returned while creating user-------> ");
					}
				});
	} 
	else {
		if(firstName!=null && firstName!='' && lastName!=null && lastName!='')
		{
				app.navigate("registration", {
					trigger : true
				});
		}
	}
  
	   
     // },
     /* "Cancel": function () {
    	  $(this).dialog("close");
          return false;
    	  app.navigate("registration", {
  			trigger : true
  		});
       
      }
    }*/
 // });
	
} 
//var r=confirm("Do you want Registered User");
//if (r==true)
 // {
	
//else
  //{
	//app.navigate("registration", {
	//	trigger : true
	//});
	//}

}


function updateUsers()
{
	 var isvalid;
	//alert(userId);
	 var securityId=$('.profileSecurity').val()+'~'+$('#securityBox').val();  
	var firstName = $("#pfirstname").val().trim();
	if(firstName!=null && firstName!='')
	{
		$("#pfirstnameDiv").attr("style", "display:none");
		$("#pfirstnameDiv").text("");
	}
	else
	{
		$("#pfirstnameDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
		$("#pfirstnameDiv").text("Field cannot be blank").css({"color":"red"});
	}
	var lastName = $("#plastname").val().trim();
	
	if(lastName!=null && lastName!='')
	{
		$("#plastnameDiv").attr("style", "display:none");
		$("#plastnameDiv").text("");
	}
	else
	{
		$("#plastnameDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
		$("#plastnameDiv").text("Field cannot be blank").css({"color":"red"});
	}
	var bio=$("#descriptionId").val().trim();
	bio=bio.replace( /[\s\n\r]+/g, ' ' );
	var emailId = $("#pemailid").val();
	var mailClass=$("#pemailid").attr('class');
	var length=emailId.length;
    var lastChar=emailId.charAt(length-1);
    var firstChar=emailId.charAt(0);
	if(emailId!=null && emailId!="")
	{
		$("#pemailidDiv").attr("style", "display:none");
		$("#pemailidDiv").text("");
		var reg =  /^(?!.*__.*)[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
		    isvalid = reg.test(emailId); 
		    if((lastChar=="_")||(lastChar==".")||(firstChar=="_")||(firstChar==".")){
	            $( "#ValidatepemailidDiv" ).attr('style','display : block;color:red;'); 
	            $( "#ValidatepemailidDiv" ).text("The first and last character of your Email should be a letter (a-z) or number."); 
	           return false;
			}
		    if(isvalid==false)
		    {
		    	if(mailClass.match('studentMail')){
					$("#pemailidDiv").attr("style", "display:none");
					$("#pemailidDiv").text("");
		    	}else{
		    	$("#ValidatepemailidDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
				$("#ValidatepemailidDiv").text("Invalid email").css({"color":"red"});
				return false;
		    	}
		    } else if(mailClass.match('studentMail')){
				$("#pemailidDiv").attr("style", "display:none");
				$("#pemailidDiv").text("");
			}else if(emailId==''){
				if(mailClass.match('studentMail')){
				}else{
					$("#ValidatepemailidDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
					$("#ValidatepemailidDiv").text("Field cannot be blank").css({"color":"red"});
					return false;
				}
			}
		    else
		    {
		    	$("#ValidatepemailidDiv").attr("style", "display:none");
				$("#ValidatepemailidDiv").text("");
		    }
		    
		   
		   
	}
	else
	{
		if(mailClass.match('studentMail')){
    		
			$("#pemailidDiv").attr("style", "display:none");
			$("#ValidatepemailidDiv").attr("style", "display:none");
			$("#pemailidDiv").text("");
    	}else{
		$("#ValidatepemailidDiv").empty();
		$("#pemailidDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
		$("#pemailidDiv").text("Field cannot be blank").css({"color":"red"});
		return false;
    	}
	}
	
	if($('.profileSecurityBox').val()==''){
		
		if(!$('.profileSecurityQuestion').attr('style').match('display:none')){
		$('#dialogempty1').empty();
		$('#dialogempty1').attr('style','display:block;color:red;');
		$('#dialogempty1').append('Field cannot be Blank');
		return false;
		}else{
		$('#dialogempty1').empty();
		}
	}else{
		$('#dialogempty1').empty();
	}

	var fbusername = $("#fbusernames").val();
	if(fbusername!=null && fbusername!='')
	{
		$("#fbusernamesDiv").attr("style", "display:none");
		$("#fbusernamesDiv").text("");
	}
	else
	{
		$("#fbusernamesDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
		$("#fbusernamesDiv").text("Field cannot be blank").css({"color":"red"});
	}
	
	
	
	  var pnoteMail=false;
	  var peventMail=false;
	  var pmemoMail=false;
	  var pcontactMail=false;
	  var pcrowdMail=false;
	  var pdueDateMail=false;
	  if($('input:checkbox[name=noteMail]:checked').val()!='' && $('input:checkbox[name=noteMail]:checked').val()=="noteBasis")
	  {
	  	pnoteMail=true;
	  }
	  if($('input:checkbox[name=eventMail]:checked').val()!='' && $('input:checkbox[name=eventMail]:checked').val()=="eventBasis")
	  {
	  	peventMail=true;
	  }
	  if($('input:checkbox[name=memoMail]:checked').val()!='' && $('input:checkbox[name=memoMail]:checked').val()=="memoBasis")
	  {
	  	 pmemoMail=true;
	  }
	  if($('input:checkbox[name=contactMail]:checked').val()!='' && $('input:checkbox[name=contactMail]:checked').val()=="contactBasis")
	  {
	  	pcontactMail=true;
	  }
	  if($('input:checkbox[name=crowdMail]:checked').val()!='' && $('input:checkbox[name=crowdMail]:checked').val()=="crowdBasis")
	  {
	  	pcrowdMail=true;
	  }
	  
	  if($('input:checkbox[name=dueDateMail]:checked').val()!='' && $('input:checkbox[name=dueDateMail]:checked').val()=="dueDateMail")
	  {
		  pdueDateMail=true;
	  }
	  else
	  {
		  pdueDateMail=false; 
	  }
	var password = $("#passwords").val();
	var timeZone=$('#ptimeZone').val();
	var confirmpassword = $("#pconfirmpasswords").val();
	var role = $("#proles").val();
	var address = $("#address1").val();
	var contactNumber = $("#pcontactNo").val();
	var skillLevel=$("#pSkillLevel").val();
	var instrument=$("#pInstrument").val();
	var favoriteMusic=$("#pfavoriteGenre").val();
	var notification= $('input:radio[name=pnotificationFlag]:checked').val();
	var noteCreateBasedOn= $('input:radio[name=notesAccessFlag]:checked').val();
	allNoteCreateBasedOn=noteCreateBasedOn;
	var role = $("#proles").val();
	var companyProfile= $('input:radio[name=companyprofile]:checked').val();
	var crowdNotification=$('input:radio[name=crowdnotificationFlag]:checked').val();
	 if(crowdNotification=="True")
	 	crowdNotification=true;
	 else
	 	crowdNotification=false;
	 var FbLogoutFlag=$('input:radio[name=fbLogoutFlag]:checked').val();
	  var tempEmail=$("#tempemailid").val();
	 //var dateofIncorparation= $("#dateofIncorparation").val();
	
	if (firstName != null && firstName != '' && lastName != null
			&& lastName != '' && fbusername != null && fbusername != '' && updateUser==false && updateUserMail==false ) {
		
		 var url = urlForServer+"user/updateEmail";
			var datastr = '{"emailId":"'+$("#pemailid").val()+'","userId":"'+userId+'"}';
		    console.log("<-------Sdatastr -------> "+ datastr);
			var params = encodeURIComponent(datastr);
			
			$.ajax({
				headers: { 
			    	"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
			    	},
			type : 'POST',
			url : url,
			data : params,
			success : function(responseText) {
			console.log("<-------data returned from url for get upate User details with reponse as -------> "+ responseText);
			var data = jQuery.parseJSON(responseText);
			  if(data!=null && data!='')
				{
				  for(var i=0;i<data.length;i++){
					  if(data[i].emailId!='empty'){
						  
					$("#ValidatepemailidDiv").text('');
				    $("#ExistspemailidDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
					$("#ExistspemailidDiv").text("Email already exists").css({"color":"red"});
					updateUserMail=true;
				  }
				  }
				}
			  else
			  {
				    $("#ExistspemailidDiv").attr("style", "display:none");
					$("#ExistspemailidDiv").text("");
					updateUserMail=false;
					
					if(emailId!='' && emailId!=null){
						 var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/ ;
						 isvalid = reg.test(emailId);
						 if(isvalid==true){
							 if(userEmail!=emailId){
							 $("#ValidatepemailidDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
							 $("#ValidatepemailidDiv").text("A verification code has been sent to your updated mail address").css({"color":"red"});
								window.setTimeout(function(){$("#ValidatepemailidDiv").text("");},10000);
							 }else{
								 $("#ValidatepemailidDiv").attr("style", "display:none");
								 $("#ValidatepemailidDiv").text("");
							 }
							}
						    else
						    {
						    	$("#ValidatepemailidDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
								$("#ValidatepemailidDiv").text("Invalid email").css({"color":"red"});
								return false;
						    	/*}
						    	$("#ValidatepemailidDiv").attr("style", "display:none");
								$("#ValidatepemailidDiv").text("");*/
						    }
						 }
					
					
//////////////////////////
					var url = urlForServer + "user/updateUser";
					var datastr = '{"userFirstName":"' + firstName + '","userLastName":"'+ lastName + '","emailId":"' + emailId + '","userName":"' + fbusername + '","userRole":"' + role 
							+ '","userId":"' + userId + '","timeZone":"'+timeZone+'","publicShareWarnMsgFlag":"' + crowdNotification + '","fbLogoutFlag":"' + FbLogoutFlag + '","notificationFlag":"' + notification + '","noteCreateBasedOn":"' + noteCreateBasedOn
							+ '","tempEmail":"' + tempEmail + '","skillLevel":"' + skillLevel + '","instrument":"' + instrument + '","favoriteMusic":"' + favoriteMusic
							+ '","contactNumber":"'+ contactNumber + '","securityId":"'+securityId+'","noteMail":"'+pnoteMail+'","eventMail":"'+peventMail+'","memoMail":"'+pmemoMail+'","contactMail":"'+pcontactMail+'","crowdMail":"'+pcrowdMail+'","dueDateMail":"'+pdueDateMail+'","url":"'+$("#urlId").val().trim()+'","description":"'+bio+'"}';
					console.log("<-------Sdatastr -------> " + datastr);
//					alert("<-------Sdatastr -------> " + datastr);
					
					var params = encodeURIComponent(datastr);
				    setTimeout(function(){},5000);
				    $.ajax({
				    	headers: { 
					    	"Mn-Callers" : musicnote,
					    	"Mn-time" :musicnoteIn				
					    	},
								type : 'POST',
								url : url,
								data : params,
								success : function(responseText) {
									
									fbLogoutFlag=FbLogoutFlag;
					    			fetchUsers();
									$('#profilemsgModal').prependTo('body').modal('show');
									//$("#loadingImg").attr('src',loadingImgUrl);
									//$("#profilemsg-header-span").attr('class',"label label-info").text("info");
									$("#profilemodal-message").text("  Creating user");
									$("#schedulestatusicon").remove();
									$("#profilemsgfooterbtn").prepend('<i class="white glyphicon glyphicon-time" id="schedulestatusicon"></i>');
									$("#profilemsgfooterbtn").attr('class','btn btn-info');
									$("#profilemsg-header-span").text("Update User Profile");
									$("#profilemodal-message").text("  User Profile Updated Successfully");
									$("#schedulestatusicon").remove();
									$("#profilemsgfooterbtn").prepend('<i class="white glyphicon glyphicon-ok" id="schedulestatusicon"></i>');
									$("#profilemsgfooterbtn").attr('class','btn btn-success');
									setTimeout(function(){
										$('#profilemsgModal').modal('hide');
									},6000);
								//alert(responseText);
						       // if(responseText=='Music Student')
						        	$('#securityQuestion').attr('style','display:none;');
						         $("#userView").text(firstName);
					             	var data = jQuery.parseJSON(responseText);
						       
								//alert("data==>"+data);
								console.log("<-------successfully returned while creating user-------> "+responseText);
									
								},
								error : function() {
								
									console
											.log("<-------Error returned while creating user-------> ");
								}

							});
						
				    ////////////////////////////////////
					
			  }
			  
			
		},
		error : function() {
			console.log("<-------error returned for update User details -------> ");
			}
		});   

			
		
		
	} 

}
var emailFlag=true;
function existsMail()
{
	var emailId= $("#emailid").val();
	if(emailId==""){
	}else{
    $("#validationEmailidDiv").attr("style", "display:none");
    $("#emailValidateError").attr("style", "display:none");
	$("#validationEmailidDiv").text("");
    var url = urlForServer+"user/getExistsEmail";
	var datastr = '{"emailId":"'+$("#emailid").val()+'"}';
    console.log("<-------Sdatastr -------> "+ datastr);
	var params = encodeURIComponent(datastr);
	
$.ajax({
	headers:{"Mn-Callers1":regMusicnote},
	type : 'POST',
	url : url,
	data : params,
	success : function(responseText) {
	console.log("<-------data returned from url for get User details with reponse as -------> "+ responseText);
	var data = jQuery.parseJSON(responseText);
	  if(data!=null && data!='')
		{
    	  $( "#dialog" ).attr('style','display : block');
    	  emailFlag=true;
		  $("#emailidDiv").attr("style", "display:none");
	      $("#emailidDiv").text(""); 
		}else
		{
			 $( "#dialog" ).attr('style','display : none');
			 emailFlag=false;
		}
	
},
error : function() {
	console.log("<-------error returned for User details -------> ");
	}
});   

	}	
	  
}
var userWarningFalg=true;
function validateUserName()
{
    $( "#userValidateError" ).empty();
    $( "#userValidateError" ).attr('style','display : none;'); 
    var letters =/^[a-zA-Z0-9._]+$/gm;  
    var userName=$("#fbusername").val();
    var length=userName.length;
    var lastChar=userName.charAt(length-1);
    var firstChar=userName.charAt(0);
    if(userName!=""){
    if(userName.match(letters))  
    { 
         if ((userName.split(".").length-1) > 1 ){
             $( "#userValidateError" ).attr('style','display : block;color:red;'); 
                $( "#userValidateError" ).text("User Name must contain single period."); 
                userWarningFalg=true;
                //return false;
         }
         if ((userName.split("_").length-1) > 1 ){
             $( "#userValidateError" ).attr('style','display : block;color:red;'); 
                $( "#userValidateError" ).text("User Name must contain single underscore."); 
                userWarningFalg=true;
                //return false;
         }
        if((lastChar=="_")||(lastChar==".")||(firstChar=="_")||(firstChar==".")){
            $( "#userValidateError" ).attr('style','display : block;color:red;'); 
            $( "#userValidateError" ).text("The first and last character of your username should be a letter (a-z) or number."); 
            userWarningFalg=true;
            //return false;  
        }
        else{
        	exitsUserName();
            userWarningFalg=false;
            return true; 
        }
     
    }  
    else  
    {  
        $( "#userValidateError" ).attr('style','display : block;color:red;'); 
        $( "#userValidateError" ).text("Please use only letters (a-z), numbers, and periods."); 
        userWarningFalg=true;
    //return false;  
    }
    }
}
var passWarningFalg=true;
function validatePassword()
{
    $( "#passValidateError" ).empty();
    $( "#passValidateError" ).attr('style','display : none;'); 
    var password=$("#password").val();
    var length=password.length;
    if(password!=""){
    if(length<6)  
    { 
        $( "#passValidateError" ).attr('style','display : block;color:red;'); 
        $( "#passValidateError" ).text("Short passwords are easy to guess. Try one with at least 6 characters."); 
        passWarningFalg=true;
        //return false;  
    }  
    else  
    {  
        passWarningFalg=false;
        //return true;  
    }
    }
}
var emailWarningFlag=true;
function validateEmail()
{
    var userRole=$('#role').val();
    var emailText = $("#emailid").val();
    var length=emailText.length;
    var lastChar=emailText.charAt(length-1);
    var firstChar=emailText.charAt(0);
    var pattern = /^(?!.*__.*)[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
  if(emailText!=""){
    if (pattern.test(emailText)) {
    	if((lastChar=="_")||(lastChar==".")||(firstChar=="_")||(firstChar==".")){
            $( "#emailValidateError" ).attr('style','display : block;color:red;'); 
            $( "#emailValidateError" ).text("The first and last character of your Email should be a letter (a-z) or number."); 
            emailWarningFlag=true;
        }else{
        	existsMail();
        	emailWarningFlag=false;
        }
        emailWarningFlag=false;
       // return true;
    } else {
    	 $( "#emailValidateError" ).empty();
    	 $("#emailidDiv").attr("style", "display:none");
    	 $("#validationEmailidDiv").attr("style", "display:none");
        $( "#emailValidateError" ).attr('style','display : block;color:red;'); 
        $( "#emailValidateError" ).text("Please enter a valid Email address."); 
        emailWarningFlag=true;
        //return false;
    }
  }else{
      if(userRole=="Music Student"){
    	  existsMail();
    	  $("#emailValidateError").attr("style", "display:none");
          emailWarningFlag=false;
          //return true;
      }else{
    	  $("#dialog").attr("style", "display:none");
    	  $("#emailValidateError").attr("style", "display:none");
            emailWarningFlag=true;
      }
  }
}
var userFalg=true;
function exitsUserName()
{
	 var UserName=$("#fbusername").val();
	    if(UserName==""){
	        $( "#userDialog" ).attr('style','display : none');
	    }else{
    var url = urlForServer+"user/ExistsUser";
	var datastr = '{"userName":"'+$("#fbusername").val()+'"}';
    console.log("<-------Sdatastr -------> "+ datastr);
	var params = encodeURIComponent(datastr);
	
$.ajax({
	headers:{"Mn-Callers1":regMusicnote},
	type : 'POST',
	url : url,
	data : params,
	success : function(responseText) {
	console.log("<-------data returned from url for get User details with reponse as -------> "+ responseText);
	var data = jQuery.parseJSON(responseText);
	  if(data!=null && data!='')
		{
		  $("#fbusernameDiv").attr("style", "display:none");
    	  $( "#userDialog" ).attr('style','display : block');
    	  userFalg=true;
		}else
		{
			 $( "#userDialog" ).attr('style','display : none');
			 userFalg=false;
		}
	
},
error : function() {
	console.log("<-------error returned for User details -------> ");
	}
});   

	
	  
}
}
var number=false;
function numberValidation()
{
	    var contactNumber = $("#pcontactNo").val();
	    var number = /[0-9]/;
	    if(contactNumber!=null && contactNumber!=''){
	    if (contactNumber.match(/^[0-9-]+$/)) {
	    	 $( "#pcontactValidationDiv" ).attr('style','display : none');
	    		$("#pcontactValidationDiv").text("");
	    	 number=true;
	    }
	    else
	    {
	    	 $( "#pcontactValidationDiv" ).attr('style','display : block');
	    		$("#pcontactValidationDiv").text("Must be numbers").css({"color":"red"});
	    	 number=false;
	    }
	}
	    
	   // var valid = number.test(contactNumber); 
	   // alert(valid);
	/*if(!isNaN(contactNumber))
	{
		 $( "#contactDiv" ).attr('style','display : none');
	}
	else
	{
		 $( "#contactDiv" ).attr('style','display : block');
		

	}*/
	
	var pincode = $("#pincode").val();
	if(!isNaN(pincode))
	{
		 $( "#pincodeDiv" ).attr('style','display : none');
	}
	else
	{
		 $( "#pincodeDiv" ).attr('style','display : block');
	}
}
function checkterms(){
	var termscheck=$('#agreeterms').is(':checked');
	if(termscheck){
		$("#termsdecline").hide();
	}else{
		$("#termsdecline").show();
		return false;
	}
	
}
function confirmPassword()
{
	var password = $("#password").val();
	var confirmpassword = $("#confirmpassword").val();
	if(password==confirmpassword)
	{
		$( "#confirmDiv" ).attr('style','display : none');
		$("#confirmpasswordDiv").attr("style", "display:none");
	    $("#confirmpasswordDiv").text("");  
	}
	else
	{
		$( "#confirmDiv" ).attr('style','display : block');
		$("#confirmpasswordDiv").attr("style", "display:none");
	    $("#confirmpasswordDiv").text("");  
	}
}
var updateUserMail=false;
function updateMail()
{
	
    var url = urlForServer+"user/updateEmail";
	var datastr = '{"emailId":"'+$("#pemailid").val()+'","userId":"'+userId+'"}';
    console.log("<-------Sdatastr -------> "+ datastr);
	var params = encodeURIComponent(datastr);
	
$.ajax({
	headers: { 
	"Mn-Callers" : musicnote,
	"Mn-time" :musicnoteIn				
	},
	type : 'POST',
	url : url,
	data : params,
	success : function(responseText) {
	console.log("<-------data returned from url for get upate User details with reponse as -------> "+ responseText);
	var data = jQuery.parseJSON(responseText);
	  if(data!=null && data!='')
		{
		  for(var i=0;i<data.length;i++){
			  if(data[i].emailId!='empty'){
		    $("#ExistspemailidDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#ExistspemailidDiv").text("Email already exists").css({"color":"red"});
			updateUserMail=true;
			  }
		  }
		}
	  else
	  {
		    $("#ExistspemailidDiv").attr("style", "display:none");
			$("#ExistspemailidDiv").text("");
			updateUserMail=false;
	  }
	  
	
},
error : function() {
	console.log("<-------error returned for update User details -------> ");
	}
});   

	
	  
}

var updateUser=false;

function updateExistsUserName()
{
	
    var url = urlForServer+"user/updateExistsUserName";
	var datastr = '{"userName":"'+$("#fbusernames").val()+'","userId":"'+userId+'"}';
    console.log("<-------Sdatastr -------> "+ datastr);
	var params = encodeURIComponent(datastr);
	
$.ajax({
	headers: { 
	"Mn-Callers" : musicnote,
	"Mn-time" :musicnoteIn				
	},
	type : 'POST',
	url : url,
	data : params,
	success : function(responseText) {
	console.log("<-------data returned from url for get upate User details with reponse as -------> "+ responseText);
	var data = jQuery.parseJSON(responseText);
	  if(data!=null && data!='')
		{
		    $("#ExistsfbusernamesDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#ExistsfbusernamesDiv").text("User Name already exists").css({"color":"red"});
			updateUser=true;
		}
	  else
	  {
		    $("#ExistsfbusernamesDiv").attr("style", "display:none");
			$("#ExistsfbusernamesDiv").text("");
			updateUser=false;
	  }
	  
	
},
error : function() {
	console.log("<-------error returned for update User details -------> ");
	}
});   

	
	  
}

function userCreationMailSend(userId){
//Ajax request for sending mail after creating user
url = urlForServer + "user/sendUserCreationMail";
datastr='{"userId":"' + userId + '"}';
params = encodeURIComponent(datastr);
$.ajax({
	headers: { 
	"Mn-Callers" : regMusicnote,
	"Mn-time" :regMusicnoteIn				
	},
	type : 'POST',
	url : url,
	data : params,
	success : function(responseText) {
},
error : function(e){
	alert("please try again later");
}
});
//////// end ////////////
}
function uploadedphoto()
{
	//alert('uploaded photo function');
	var url = urlForServer+"user/getUserDetails";
	var datastr = '{"userId":"'+userId+'"}';
    console.log("<-------Sdatastr -------> "+ datastr);
	var params = encodeURIComponent(datastr);
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
		"Mn-time" :musicnoteIn				
		},
		type : 'POST',
		url : url,
		data : params,
		success : function(responseText) {
		console.log("<-------data returned from url for get User details with reponse as -------> "+ responseText);
		var data = jQuery.parseJSON(responseText);
		  if(data!=null && data!='')
			{
				for ( var i = 0; i < data.length; i++) {
					 userDetailsJsonObj = data[i];
					 var profileImg="";
					 if(userDetailsJsonObj['filePath']!='null' && userDetailsJsonObj['filePath']!='')
					 {
					 profileImg=uploadUrl+userDetailsJsonObj['filePath'];
					 //alert(profileImg);
						 $("#profileImg").attr("src",profileImg );
						 $("#removePictureId").attr("style", "display:block;");
					 }
					 else
					 {
						 $("#profileImg").attr("src", "pics/dummy.jpg");
						 $("#removePictureId").attr("style", "display:none");
						 
					 }
				}
			}
			},
			error : function() {
				console.log("<-------error returned for User details -------> ");
				}
			});
}
