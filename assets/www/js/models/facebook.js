  // Defaults to sessionStorage for storing the Facebook token
    // openFB.init({appId: '1466485926948895'});
    // openFB.init({appId: '541136035945063'});
    //  Uncomment the line below to store the Facebook token in localStorage instead of sessionStorage
  openFB.init({appId: '541136035945063', tokenStore: window.localStorage});

    function loginFB() {
        openFB.login(
                function(response) {
                    if(response.status === 'connected') {
                    	FBuserLogin=true;
                    getInfo();
                    } else {
                        alert('Facebook login failed: ' + response.error);
                    }
                }, {scope: 'email,read_stream,publish_stream'});
    }

    function getInfo() {
        openFB.api({
            path: '/me',
            success: function(response) {
                console.log(JSON.stringify(response));
                    if(response.name!=undefined){
                        var userName=response.name;
                        var emailId=response.email;
                        var facebookID=response.id;
                    	LoginAuthenticationViaFB(userName,facebookID,emailId,response);
                    }
            },
            error: errorHandler});
    }

    function share() {
        openFB.api({
            method: 'POST',
            path: '/me/feed',
            params: {
                message: document.getElementById('Message').value || 'Testing Facebook APIs'
            },
            success: function() {
                alert('the item was posted on Facebook');
            },
            error: errorHandler});
    }

    function revoke() {
        openFB.revokePermissions(
                function() {
                    alert('Permissions revoked');
                },
                errorHandler);
    }

    function logoutFB() {
        openFB.logout(
        		function(response) {
                 // alert('log outttt');
                },
                errorHandler);
    }

    function errorHandler(error) {
        alert(error.message);
    }
