window.Employee = Backbone.Model.extend({

    urlRoot:"../api/employees",

    initialize:function () {
        this.reports = new EmployeeCollection();
        this.reports.url = '../api/employees/' + this.id + '/reports';
    }

});

