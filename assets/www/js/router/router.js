window.Router = Backbone.Router.extend({

    routes: {
	    "home": "home",
	    "": "login",
	    "logout":"logout",
	    "cancel":"cancel",
	    "registration": "registration",
	    "userLogin":"userLogin",
        "profile": "profile",
        "notes": "notes",
        "memos": "notes",
        "music": "notes",
        "schedule": "notes",
        "messages": "messages",
        "reminders": "reminders",
        "recordlesson": "recordlesson",
        "students": "students",
        "invoice": "invoice",
        "employees/:id": "employeeDetails",
		"calendar":"calendar",
		"crowd":"crowd",
		"resetpassword":"resetpassword",
		"help":"help",
		"crowdProfiles":"crowdProfiles"
    },

    initialize: function () {
        this.headerView = new HeaderView();
        $("#header").html(this.headerView.render().el);

        this.sideBar = new SideBar();
        $("#sideBar").html(this.sideBar.render().el);
        
        this.footerView = new FooterView();
        $("#footer").html(this.footerView.render().el);
        
        // Close the search dropdown on click anywhere in the UI
        $('body').click(function () {
            $('.dropdown').removeClass("open");
        });
        
    },
    login:function(){
    	
    	   if (!this.loginView) {
               this.loginView = new LoginView();
               this.loginView.render();
           } else {
               this.loginView.delegateEvents(); // delegate events when the view is recycled
           }
           $("#main").html(this.loginView.el);
            //createUserTest();
          
           //fetchMailMessages("inbox");
    },
    registration:function(){
    	  if (!this.registrationView) {
              this.registrationView = new RegistrationView();
              this.registrationView.render();
          } else {
              this.registrationView.delegateEvents(); // delegate events when the view is recycled
          }
          $("#main").html(this.registrationView.el);
          $("#role").empty();
          $("#userfirstname").val("");
			$("#userlastname").val("");
			$("#emailid").val("");
			$("#agreeterms").attr('checked', false); 
			$("#fbusername").val("");
			$("#password").val("");
			$("#confirmpassword").val("");
			$("#securityBox").val("");
			$("#dialogempty1").val("");
			regMusicnoteGenerator();
    },
    /*createUser:function(){
    	createUser();
    },*/
    userLogin:function(){
    	userLogin();
    },
    logout:function(){
    	 if(FBuserLogin){
    		 userLogoutModel();
    	 }else{
    		 userLogout(); 
    	}
    },
    cancel:function(){
    	userLogout();
},
    home: function () {
        // Since the home view never changes, we instantiate it and render it only once
	     //fetchUsers();
        // alert(document.getElementById('userName').value);
        if (!this.homeView) {
        	this.homeView = new HomeView();
            this.homeView.render();
            
             
        } else {
            this.homeView.delegateEvents(); // delegate events when the view is recycled
        }
        $("#content").html(this.homeView.el);
        this.headerView.select('home-menu');
		getrecentActivity();
		fetchNotifications();
		fetchRemainders();
		$('#welcomemsg').attr('style','display:none');
		$("#searchUser").val("");
		$("#searchMemoNote").val('');
		$("#searchScheduleNote").val('');
		$("#searchMusicNote").val('');
		//uesrName();
    },

    profile: function () {
        if (!this.ProfileView) {
            this.ProfileView = new ProfileView();
            this.ProfileView.render();
        }
        $('#content').html(this.ProfileView.el);
        this.headerView.select('profile-menu');
        //$('#msgLoadingModal').modal('show');
        userProfile();
        fetchUsers();
        $("#searchUser").val("");
		$("#searchMemoNote").val('');
		$("#searchScheduleNote").val('');
		$("#searchMusicNote").val('');
        $('#plastnameDiv').attr('style','display:none;');
        $('#pfirstnameDiv').attr('style','display:none;');
        $('#dialogempty1').attr('style','display:none;');
        $('#ValidatepemailidDiv').attr('style','display:none;');
        $('#ExistspemailidDiv').attr('style','display:none;');
        $('#ValidatepemailidDiv').attr('style','display:none;');
        $('#pemailidDiv').attr('style','display:none;');
        $('.profileSecurity').val('0');
        $('.profileSecurityBox').val('');
        $('.profileSecurityBox').attr('disabled',true);
        $('#welcomemsg').attr('style','display:none');
        $("#bioVarning").attr("style", "dispaly:none");
		$("#bioVarning").text("");
		//uesrName();
    },
	
    notes: function () {
        if (!this.notesView) {
            this.notesView = new NotesView();
            this.notesView.render();
            window.setTimeout(function(){
    		window.scrollTo(0,0);},3000);
        }
        $("#searchUser").val("");
		$("#searchMemoNote").val('');
		$("#searchScheduleNote").val('');
		$("#searchMusicNote").val('');
        $('#content').html(this.notesView.el);
        $('#welcomemsg').attr('style','display:none');
    },
    messages: function () {
        if (!this.messagesView) {
            this.messagesView = new MessagesView();
            this.messagesView.render();
        }
        
        $('#content').html(this.messagesView.el);
        $('#welcomemsg').attr('style','display:none');
       
    },
    reminders: function () {
        if (!this.remindersView) {
            this.remindersView = new RemindersView();
            this.remindersView.render();
        }
        $('#content').html(this.remindersView.el);
        $('#welcomemsg').attr('style','display:none');
    },
    recordlesson: function () {
        if (!this.recordlessonView) {
            this.recordlessonView = new RecordLessonView();
            this.recordlessonView.render();
        }
        $("#searchUser").val("");
		$("#searchMemoNote").val('');
		$("#searchScheduleNote").val('');
		$("#searchMusicNote").val('');
        $('#content').html(this.recordlessonView.el);
        $('#welcomemsg').attr('style','display:none');
    },
    students: function () {
        if (!this.studentsView) {
            this.studentsView = new StudentsView();
            this.studentsView.render();
        }
        $("#searchUser").val("");
		$("#searchMemoNote").val('');
		$("#searchScheduleNote").val('');
		$("#searchMusicNote").val('');
        $('#content').html(this.studentsView.el);
        $('#welcomemsg').attr('style','display:none');
    },
    invoice: function () {
        if (!this.invoiceView) {
            this.invoiceView = new InvoiceView();
            this.invoiceView.render();
        }
        $('#content').html(this.invoiceView.el);
        $('#welcomemsg').attr('style','display:none');
       
        fetchTeacher();
		fetchStudent();
		var currentDate = new Date()
	    var day = currentDate.getDate()
	    var month = currentDate.getMonth() + 1
	    var year = currentDate.getFullYear()
	    var dateValue=day + "-" + month + "-" + year ;
	    $("#invoiceDate").text(dateValue);
		$("#dateTerm").val(dateValue);
	  	var invoiceNo=1000;
		$("#invoiceNo").text(invoiceNo);

    },
    employeeDetails: function (id) {
        var employee = new Employee({id: id});
        employee.fetch({
            success: function (data) {
                // Note that we could also 'recycle' the same instance of EmployeeFullView
                // instead of creating new instances
                $('#content').html(new EmployeeView({model: data}).render().el);
                $('#welcomemsg').attr('style','display:none');
            }
        });
    },
	calendar:function(){
    	   if (!this.calendarView) {
               this.calendarView = new CalendarView();
               this.calendarView.render();
           } else {
               this.calendarView.delegateEvents(); // delegate events when the view is recycled
           }
           $("#content").html(this.calendarView.el);
           $('#welcomemsg').attr('style','display:none');
    },
    crowd:function(){
 	   if (!this.crowdView) {
            this.crowdView = new CrowdView();
            this.crowdView.render();
        } else {
            this.crowdView.delegateEvents(); // delegate events when the view is recycled
        }
        $("#content").html(this.crowdView.el);
        //voteLists();
       	newNotes();
		$('#welcomemsg').attr('style','display:none');
		$("#searchUser").val("");
		$("#searchMemoNote").val('');
		$("#searchScheduleNote").val('');
		$("#searchMusicNote").val('');
        //publicNotes();
        /*$('#newNote').empty();
        $('#mostView').empty();
        $('#mostPopular').empty();*/
	},
    help:function(){
	  	  if (!this.helpView) {
	            this.helpView = new HelpView();
	            this.helpView.render();
	        } else {
	            this.helpView.delegateEvents(); // delegate events when the view is recycled
	        }
	  	$("#searchUser").val("");
		$("#searchMemoNote").val('');
		$("#searchScheduleNote").val('');
		$("#searchMusicNote").val('');
	        $("#content").html(this.helpView.el);
	        $("#userfeedback").val(""); 
	        $("#limitChar1").hide();
			$("#limitChar").attr("style", "dispaly:block;font-family: Helvetica Neue;font-size:14px;");
			$("#limitChar").text("600 characters remaining");
			viewHelpVideoUploaded();
	  },
	resetpassword:function(){
		if (!this.resetView) {
            this.resetView = new ResetView();
            this.resetView.render();
        } else {
            this.resetView.delegateEvents(); // delegate events when the view is recycled
        }
        $("#content").html(this.resetView.el);
        $("#oldPasswordDiv").attr("style", "dispaly:none");
		$("#oldPasswordDiv").text("");
		$("#oldPasswordDiv1").attr("style", "dispaly:none");
		$("#oldPasswordDiv1").text("");
		$("#newpasswordDiv").attr("style", "dispaly:none");
		$("#newpasswordDiv").text("");
		$("#cfrmpasswordDiv").attr("style", "dispaly:none");
		$("#cfrmpasswordDiv").text("");
		$("#newpassword").val("");
		$("#oldPassword").val("");
		$("#cfrmpasswordDiv1").attr("style", "dispaly:none");
		$("#cfrmpasswordDiv1").text("");
		$("#cfrmpassword").val("");
		$('#welcomemsg').attr('style','display:none');
		$("#searchUser").val("");
		$("#searchMemoNote").val('');
		$("#searchScheduleNote").val('');
		$("#searchMusicNote").val('');
		$("#resetPasswordNotSupport").attr("style", "dispaly:none");
		$("#resetPasswordNotSupport").text("");
	},
	crowdProfiles: function () {
        if (!this.crowdProfile) {
            this.crowdProfile = new crowdProfile();
            this.crowdProfile.render();
        }
        $('#content').html(this.crowdProfile.el);
        this.headerView.select('crowdProfiles-menu');
        $("#main").attr("style", "display:none");
        $("#content").html(this.crowdProfile.el);
    }
 
});

