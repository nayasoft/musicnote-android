$(document).ready(
		function() {
			$('#contactPicker').click(function() {
				$('#contactPickerspan').attr('style', 'display:none');
				contactPicker();
			});
			$("#createCallList").click(
					function() {

						var contact = "";
						var id;
						var contactName = "";
						var contactNumber = "";
						var ret = "";
						var datastr = "";
						contact = $("#contactLister").jqGrid('getGridParam',
								'selarrrow');
						for (id in contact) {
							ret = $("#contactLister").jqGrid('getRowData',
									contact[id]);
							contactName = contactName + ret.Name + ",";
							contactNumber = contactNumber + ret.Phone1 + "/"
									+ ret.Phone2 + ",";
						}
						if (contact == "") {

							alert("Please select atleast one contact")
						} else {
							datastr = contactNumber + ":" + contactName
									+ ":ContactManagerPhone:1111111111:1";
							console.log("datastr=" + datastr);
							contactListCreationCM(datastr);
						}
					});
		});

function contactListCreationCM(datastr) {
	var url = urlForServer + "CallList/Create/"+loginUserId +"/"
			+gcLoginUserId+"/"+gcUserName+"/"+gcPassword;
	var params = encodeURIComponent(datastr);
	$
			.ajax({
				type : 'POST',
				url : url,
				data : params,
				success : function(responseText) {
					console
							.log("<-------Scuccessfully created call list From Contact Manager -------> "
									+ responseText);
				},
				error : function() {
					alert("Error while creating call list from Contact");
				}
			});

}

function contactPicker() {
	var options = new ContactFindOptions();
	options.filter = "";
	options.multiple = true;
	var fields = [ "*" ];
	navigator.contacts.find(fields, onSuccess, onError, options);
}

function onSuccess(contacts) {
	var JsonString = "";
	var name = "";
	var tempData = "";
	for ( var i = 0; i < contacts.length; i++) {
		if (contacts[i].displayName != null)
			name = contacts[i].displayName;
		var data = ""
		var phoneNumber1 = "";
		var phoneNumber2 = "";
		var phoneNumber3 = "";
		var phoneNumber4 = "";
		var phoneNumber5 = "";
		var len = "";
		if (contacts[i].phoneNumbers != null && name != "") {
			len = contacts[i].phoneNumbers.length;
			if (len >= 1)
				phoneNumber1 = contacts[i].phoneNumbers[0].value;
			if (len >= 2)
				phoneNumber2 = contacts[i].phoneNumbers[1].value;
			if (len >= 3)
				phoneNumber3 = contacts[i].phoneNumbers[2].value;
			if (len >= 4)
				phoneNumber4 = contacts[i].phoneNumbers[3].value;
			if (len >= 5)
				phoneNumber5 = contacts[i].phoneNumbers[4].value;
			data = '{"Name":"' + name + '","Phone1":"' + phoneNumber1
					+ '","Phone2":"' + phoneNumber2 + '","Phone3":"'
					+ phoneNumber3 + '","Phone4":"' + phoneNumber4
					+ '","Phone5":"' + phoneNumber5 + '"}';
			console.log("data = " + data);
			tempData = data + "," + tempData;
		}
	}
	JsonString = "[" + tempData + "]";
	JsonString = JsonString.replace("},]", "}]");
	JsonString = JsonString.replace(") ", "");
	JsonString = JsonString.replace("(", "");
	JsonString = JsonString.replace("-", "");
	console.log("JsonString = " + JsonString);
	var jsonobject = JSON.parse(JsonString);

	$("#contactLister").jqGrid({
		data : jsonobject,
		datatype : "local",
		colNames : [ 'Name', 'Phone#1', 'Phone#2' ],
		colModel : [ {
			name : 'Name',
			index : 'Name',
			width : 55
		}, {
			name : 'Phone1',
			index : 'Phone1',
			width : 90
		}, {
			name : 'Phone2',
			index : 'Phone2',
			width : 100
		} ],
		sortname : 'Name',
		recordpos : 'left',
		viewrecords : true,
		sortorder : "asc",
		multiselect : true,
		caption : "Contact Picker"
	});
	$('#createCallListdiv').attr('style', 'display:block');
}
function onError(contactError) {
	alert('onError!');
}

// file dialog
var successCallBack = function(args) {
                 var x = JSON.parse(args);
				  if(x.status=='success')
				  {
	console.log(JSON.stringify(args));
	}
	else
	{
	alert(x.status);
	}

};
var failCallBack = function(args) {
	console.log(JSON.stringify(args));
	alert("Error occurred while uploading,Please try again now");
};
var successCallBackFile = function(args) {

//alert(JSON.stringify(args));
	console.log(JSON.stringify(args));
	document.getElementById("filename").value = JSON.stringify(args);
};
